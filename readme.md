# [Curriculum Vitae](https://www.linkedin.com/in/mraxilus)

🏆 _Outlining the course of the life of Emmanuel Smith._

## 🔮 Purpose

The purpose of this project is to provide an account of my experience and qualifications.
This includes education, work experiences, achievements, and awards.
Please be aware, this CV only reflects my achievements up to the point of the latest commit.

## 🧰 Usage

In the interest of simplifying updates, this project adheres to the [JSON Resume](https://www.uwe.ac.uk/study/study-support/peer-assisted-learning) specification.
Their [command line application](https://www.npmjs.com/package/resume-cli) make publishing this CV is a straightforward process.
For example, to produce the PDF version of my CV, run:

```{sh}
npm install
$(npm bin)/resume export resume_raw.pdf
```

## ⚖️ License

Copyright ©️ Emmanuel M. Smith.
I license this project under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

You may negociate a waiver for the non-commercial restriction.
Please contact me at legal@projectaxil.us to do so.
